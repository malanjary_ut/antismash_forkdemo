# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2012 Daniyal Kazempour
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# Copyright (C) 2012,2013 Kai Blin
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
'''
More detailed thiopeptide analysis using HMMer-based leader peptide
cleavage site prediction as well as prediction of molcular mass.
'''

import logging
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from antismash import utils
import re



class Thiopeptide(object):
    '''
    Class to calculate and store thiopeptide information
    '''
    def __init__(self, start, end, score):
        self.start = start
        self.end = end
        self.score = score
        self.thio_type = ''
        self._leader = ''
        self._core = ''
        self._weight = -1
        self._monoisotopic_weight = -1
        self._alt_weights = []
        self._macrocycle = ''
        self._amidation = False
        self._mature_alt_weights = []
        self._mature_features = ''
        self._c_cut = ''


    @property
    def core(self):
        return self._core

    @core.setter
    def core(self, seq):
        seq = seq.replace('X', '')
        self.core_analysis_monoisotopic = ProteinAnalysis(seq, monoisotopic=True)
        self.core_analysis = ProteinAnalysis(seq, monoisotopic=False)
        self._core = seq

    @property
    def leader(self):
        return self._leader

    @leader.setter
    def leader(self, seq):
        self._leader = seq

    @property
    def macrocycle(self):
        self._predict_macrocycle()
        return self._macrocycle

    @macrocycle.setter
    def macrocycle(self, macro):
        self._macrocycle = macro

    def _predict_macrocycle(self):
        '''
        Prediction of macrocycle ring
        '''
        
        if not self._core:
            raise ValueError()


        aux = self._core
        if len(self._core) == 17 and self._core[0] in "TIV":
            aux = self._core[4:]

        if aux[0] == "S":
            if "SC" in aux[9:12]:
                         
                if aux[9] == "S":
                    self._macrocycle = "26-member"
            
                if aux[10] == "S":
                    self._macrocycle = "29-member"

            if re.search(r"S{6,8}?",aux[(len(aux)/2):]):
                if aux[12] =="S":
                    self._macrocycle = "35-member"
                
    @property
    def amidation(self):
        return self._amidation

    @amidation.setter
    def amidation(self, value):
        self._amidation = value

    @property
    def mature_features(self):
        self._predict_mature_core_features()
        return self._mature_features

    @mature_features.setter
    def mature_features(self, feats):
        self._mature_features = feats

        
    def _predict_mature_core_features(self):
        '''
        Prediction of some features of the mature peptide
        '''
        if not self.thio_type:
            raise ValueError()

        self._mature_features = ''
        if self.thio_type == 'Type-I':
            self._mature_features = "Central ring- pyridine tetrasubstituted (hydroxyl group present); second macrocycle"
        
        if self.thio_type == 'Type-III':
            self._mature_features = "Central ring- pyridine trisubstituted"
        
        if self.thio_type == 'Type-II':
            self._mature_features = "Central ring- piperidine; second macrocycle containing a quinaldic acid moiety"
    
        return self._mature_features

    @property
    def c_cut(self):
        return self._c_cut

    @c_cut.setter
    def c_cut(self, ccut):
        self._c_cut = ccut

    
    def __repr__(self):
        return "Thiopeptide(%s..%s, %s, %r, %r, %s(%s), %s, %s, %s, %s)" % (self.start, self.end, self.score, self._core, self.thio_type, self._monoisotopic_weight, self._weight, self.macrocycle, self.amidation, self._mature_features, self.c_cut)
        
   
    def _calculate_mw(self):
        '''
        (re)calculate the monoisotopic mass and molecular weight
        '''
        if not self._core:
            raise ValueError()

       
        aas = self.core_analysis.count_amino_acids()
        no_thr_ser = aas['T'] + aas['S']
        no_cys = aas['C']

        mol_mass = self.core_analysis.molecular_weight()
        monoisotopic_mass = self.core_analysis_monoisotopic.molecular_weight()
        alt_weights = []
        self._weight = mol_mass 
        self._monoisotopic_weight = monoisotopic_mass
        
        dhs = 18.02 * (no_thr_ser-1)

        # every unbridged Ser or Thr might not be dehydrated
        for i in range(1, no_thr_ser + 1):
            self._alt_weights.append(mol_mass + 18.02 * i)

        if self.thio_type != "Type-III":
        # maturation reactions:
            mol_mass -= dhs
            
            #Ycao > cys/ser to azole ~20 Da for ring
            mol_mass -=  (20 * (no_cys - 1))
        
            #cycloaddition of 2 Dha residues
            mol_mass -= 35
        
            if self.thio_type == 'Type-I':
                #indolic acid (172) + cyclization
                mol_mass += 172 - 3

            if self.thio_type == 'Type-II':
                #quinaldic acid + cyclization
                mol_mass += 215 
            
            #considering 2 oxidations
            mol_mass += 32

        if self.amidation == True:
            mol_mass -= 70


        mass_diff = self._weight - mol_mass
        mod_mol_mass = mol_mass
        mod_monoisotopic_mass = monoisotopic_mass - mass_diff
 
        self._mature_alt_weights.append (mod_mol_mass)
        self._mature_alt_weights.append(mod_monoisotopic_mass)

       
        # every unbridged Ser or Thr might not be dehydrated
        for i in range(1, no_thr_ser + 1):
            self._mature_alt_weights.append(mod_mol_mass + 18.02 * i)

            
    @property
    def monoisotopic_mass(self):
        '''
        function determines the weight of the core peptide and substracts
        the weight which is reduced, due to dehydratation
        '''
        if self._monoisotopic_weight > -1:
            return self._monoisotopic_weight

        self._calculate_mw()
        return self._monoisotopic_weight

    @property
    def molecular_weight(self):
        '''
        function determines the weight of the core peptide
        '''
        if self._weight > -1:
            return self._weight

        self._calculate_mw()
        return self._weight

    @property
    def alternative_weights(self):
        '''
        function determines the possible alternative weights assuming one or
        more of the Ser/Thr residues aren't dehydrated
        '''
        if self._alt_weights != []:
            return self._alt_weights

        self._calculate_mw()
        return self._alt_weights

    @property
    def mature_alt_weights(self):
        '''
        function determines the possible mature alternative weights which includes mw,
        monoisotopic mass and alternative weightss due to different hydrated residues
    
        '''
        if self._mature_alt_weights != []:
            return self._mature_alt_weights

        self._calculate_mw()
        return self._mature_alt_weights

    
def predict_amidation(found_domains):
    amidation = False
    # nosA homologs nocA, tpdK nocA berI pbt
    if 'thio_amide' in found_domains:
        amidation = True

    return amidation
    
    
def predict_cleavage_site(query_hmmfile, target_sequence):
    '''
    Function extracts from HMMER the start position, end position and score 
    of the HMM alignment
    '''
    hmmer_res = utils.run_hmmpfam2(query_hmmfile, target_sequence)
    resvec = None
    
    for res in hmmer_res:
        for hits in res:
            thio_type = hits.description
            for hsp in hits:
                resvec = Thiopeptide(hsp.query_start, hsp.query_end-14, hsp.bitscore)
                return resvec
    
    return resvec

        
def predict_type_from_gene_cluster(found_domains):
    '''
    Predict the thiopeptide type from the gene cluster
    '''

    if 'PF06968' in found_domains:
        return 'Type-I'
        
    if 'PF04055' in found_domains or 'PF00733' in found_domains:
        return 'Type-II'
    
    return 'Type-III'


          
def features_detected(seq_record, cluster):
    '''
    Identifies the detected features
    '''
    
    features= []
    for feature in utils.get_cds_features(seq_record):
        if feature.location.start < cluster.location.start or \
           feature.location.end > cluster.location.end:
            continue

        features.append(feature)
       
    return features

def get_detected_domains(seq_record, cluster):
    
    found_domains = []
    for feature in utils.get_cds_features(seq_record):
        if feature.location.start < cluster.location.start or \
           feature.location.end > cluster.location.end:
            continue
      
        if not 'sec_met' in feature.qualifiers:
            continue

        for entry in feature.qualifiers['sec_met']:
            if entry.startswith('Domains detected:'):
                entry = entry[17:]
                domains = entry.split(';')
                for domain in domains:
                    found_domains.append(domain.split()[0])

    return found_domains

thresh_hit = -2

def run_thiopred(seq_record, query, thio_type):
    hmmer_profile = 'thio_cleave.hmm'
   
    query_sequence = utils.get_aa_sequence(query, to_stop=True)
    thio_a_fasta = ">%s\n%s" % (utils.get_gene_id(query), query_sequence)
  
    #run sequence against profile and parse them in a vector containing START, END and SCORE
    profile = utils.get_full_path(__file__, hmmer_profile)
    result = predict_cleavage_site(profile, thio_a_fasta)
    result.thio_type = thio_type
 
    
    if result is None:
        logging.debug('%r: No cleavage site predicted' % utils.get_gene_id(query))
        return

    if thresh_hit > result.score:
        logging.debug('%r: Score %0.2f below threshold %0.2f' %
                      (utils.get_gene_id(query), result.score, thresh_hit))
        return

    
    #extract now the core peptide
    result.leader = query_sequence[:result.end]
    result.core = query_sequence[result.end:]
    
   
    if not 'sec_met' in query.qualifiers:
        query.qualifiers['sec_met'] = []


    if ";".join(query.qualifiers['sec_met']).find(';Kind: biosynthetic') < 0:
        query.qualifiers['sec_met'].append('Kind: biosynthetic')

    # leader cleavage "validation"
    pep_hmmer_profile = 'thiopep2.hmm'     
    thresh_pep_hit = -2

    core_a_fasta = ">%s\n%s" % (utils.get_gene_id(query), result.core)
 
    profile_pep = utils.get_full_path(__file__, pep_hmmer_profile)
    hmmer_res_pep = utils.run_hmmpfam2(profile_pep, core_a_fasta)

    filter_out = True
    for res in hmmer_res_pep:
        for hits in res:
            for seq in hits:
                if seq.bitscore > thresh_pep_hit:
                    filter_out = False

    if filter_out:
        return

    # additional filter(s) for peptide prediction
    aux = ""
    if re.search("[ISTV][SACNTW][STNCVG][ATCSGM][SVTFC][CGSTEAV][TCGVY]", result.core):
        aux = re.search("[ISTV][SACNTW][STNCVG][ATCSGM][SVTFC][CGSTEAV][TCGVY].*"\
                        ,result.core).group()
    else:
        return
    
    diff = len(result.core)-len(aux)
    
    if aux != "" and (len(aux)<20 and len(aux)>10):
        result.leader = result.leader+result.core[:diff]
        result.core = aux

        
    # prediction of cleavage in C-terminal based on thiopeptide's core sequence
    # if last core residue != S or T or C > great chance of a tail cut
    if result.core[-1] not in "SCT":
        C_term_hmmer_profile = 'thio_tail.hmm'
        thresh_C_hit = -9

        temp = result.core[-10:]
        core_a_fasta = ">%s\n%s" % (utils.get_gene_id(query),temp)
 
        profile_C = utils.get_full_path(__file__, C_term_hmmer_profile)
        hmmer_res_C = utils.run_hmmpfam2(profile_C, core_a_fasta)

        for res in hmmer_res_C:
            for hits in res:
                for seq in hits:
                    if seq.bitscore > thresh_C_hit:
                        result.c_cut = temp[seq.query_end-1:]
       
    
    if result is None:
        logging.debug('%r: No C-terminal cleavage site predicted' % utils.get_gene_id(query))
        return


    if not 'sec_met' in query.qualifiers:
        query.qualifiers['sec_met'] = []


    if ";".join(query.qualifiers['sec_met']).find(';Kind: biosynthetic') < 0:
        query.qualifiers['sec_met'].append('Kind: biosynthetic')

    return result
     

def result_vec_to_features(orig_feature, res_vec):
    start = orig_feature.location.start
    end = orig_feature.location.start + (res_vec.end * 3)
    strand = orig_feature.location.strand
    loc = FeatureLocation(start, end, strand=strand)
    leader_feature = SeqFeature(loc, type='CDS_motif')
    leader_feature.qualifiers['note'] = ['leader peptide']
    leader_feature.qualifiers['note'].append('thiopeptide')
    leader_feature.qualifiers['note'].append('predicted leader seq: %s' % res_vec.leader)
    leader_feature.qualifiers['locus_tag'] = [ utils.get_gene_id(orig_feature) ]
    start = end
    end = orig_feature.location.end
    loc = FeatureLocation(start, end, strand=strand)
    core_feature = SeqFeature(loc, type='CDS_motif')
    core_feature.qualifiers['note'] = ['core peptide']
    core_feature.qualifiers['note'].append('thiopeptide')

           
    #resets .. to recalculate weights considering C-terminal putative cut
    res_vec._weight = -1
    res_vec._monoisotopic_weight = -1
    oldcore = res_vec.core
    res_vec.core = oldcore[:(len(res_vec.core)-len(res_vec.c_cut))]
 

    core_feature.qualifiers['note'].append('monoisotopic mass: %0.1f' % res_vec.monoisotopic_mass)
    core_feature.qualifiers['note'].append('molecular weight: %0.1f' % res_vec.molecular_weight)
    if res_vec.alternative_weights:
        weights = map(lambda x: "%0.1f" % x, res_vec.alternative_weights)
        core_feature.qualifiers['note'].append('alternative weights: %s' % "; ".join(weights))

    if res_vec.thio_type != 'Type-III':
    #weights considering maturation reaction
        weights = map(lambda x: "%0.1f" % x, res_vec.mature_alt_weights)
        core_feature.qualifiers['note'].append('alternative weights considering maturation: %s' % "; ".join(weights))

       
    core_feature.qualifiers['note'].append('predicted core seq: %s' % res_vec.core)
    core_feature.qualifiers['note'].append('predicted type: %s' % res_vec.thio_type)
    core_feature.qualifiers['note'].append('score: %0.2f' % res_vec.score)

    
    core_feature.qualifiers['note'].append('predicted macrocycle: %s' %res_vec.macrocycle)
    core_feature.qualifiers['note'].append('putative cleaved off residues: %s' %res_vec.c_cut)


    if res_vec.amidation:
        core_feature.qualifiers['note'].append('predicted tail reaction: dealkylation of C-Terminal residue; amidation')
    core_feature.qualifiers['note'].append('predicted core features: %s' %res_vec.mature_features)    
    core_feature.qualifiers['locus_tag'] = [ utils.get_gene_id(orig_feature) ]

    return [leader_feature, core_feature]


def specific_analysis(seq_record, options):

 
    clusters = utils.get_cluster_features(seq_record)
    for cluster in clusters:
        if 'product' not in cluster.qualifiers or \
           'thiopeptide' not in cluster.qualifiers['product'][0]:
            continue
               
        thio_fs = features_detected(seq_record, cluster)
        domains = get_detected_domains(seq_record, cluster)
        thio_type = predict_type_from_gene_cluster(domains)
        amidation = predict_amidation(domains)
       
        if thio_type is None:
            return
    
        for thio_f in thio_fs:
            result_vec = run_thiopred(seq_record, thio_f, thio_type)
            
            if result_vec is None:
                continue

            if amidation:
                result_vec.amidation = True
            
                
            new_features = result_vec_to_features(thio_f, result_vec)
            seq_record.features.extend(new_features)
            


          
            
